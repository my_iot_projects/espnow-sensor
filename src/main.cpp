//define SERVER
//#define ASYNC_MQTT

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "EspNowHelper.h"
#include "Leds.h"

const uint8_t LED_PIN = 2;
const bool LED_LEVEL = LOW;

static const uint8_t ESPNOW_MAGIC = 0xA5;

enum espnow_type_t : uint8_t { ESPNOW_ACK, ESPNOW_DATA };

struct __packed espnow_header_t {
  uint8_t magic; // Must be == ESPNOW_MAGIC (0xA5)
  espnow_type_t type;
  uint16_t num;
};

struct __packed payload_t {
  uint32_t id;
  uint32_t uptime;
};

struct __packed espnow_data_t {
  espnow_header_t header;
  payload_t payload;
};

class EspNowClientPlus : public EspNowClient {
public:
  EspNowClientPlus(uint8_t channel, const uint8_t *mac) : EspNowClient(channel, mac), _num(0) {}

protected:
  void onReceive(const uint8_t *mac, const uint8_t *data, uint8_t len);

  bool isAckPacket(const uint8_t *data, uint8_t len);
  bool sendData();

  uint16_t _num;

  friend void loop();
};

Led *led = NULL;
EspNowGeneric *esp_now = NULL;

static String macToString(const uint8_t mac[]);

static void dumpPacket(const uint8_t *data, uint8_t len) {
  bool error = true;

  if (len == sizeof(espnow_header_t)) {
    if ((((espnow_header_t*)data)->magic == ESPNOW_MAGIC) && (((espnow_header_t*)data)->type == ESPNOW_ACK)) {
      Serial.print(F("ESP-NOW ACK packet (#"));
      Serial.print(((espnow_header_t*)data)->num);
      Serial.println(')');
      error = false;
    }
  } else if (len == sizeof(espnow_data_t)) {
    if ((((espnow_data_t*)data)->header.magic == ESPNOW_MAGIC) && (((espnow_data_t*)data)->header.type == ESPNOW_DATA)) {
      Serial.print(F("ESP-NOW DATA packet (#"));
      Serial.print(((espnow_data_t*)data)->header.num);
      Serial.println(')');
      error = false;
    }
  }
  if (error) {
    Serial.println(F("Wrong ESP-NOW packet!"));
  }
}

void EspNowClientPlus::onReceive(const uint8_t *mac, const uint8_t *data, uint8_t len) {
  Serial.print(F("\nESP-NOW packet received from "));
  Serial.println(macToString(mac));
  dumpPacket(data, len);
  if (isAckPacket(data, len)) {
    if (((espnow_header_t*)data)->num == _num) {
      _received = true;
    } else {
      Serial.println(F("Wrong num in header!"));
    }
  }
}

bool EspNowClientPlus::isAckPacket(const uint8_t *data, uint8_t len) {
  return (len == sizeof(espnow_header_t)) && (((espnow_header_t*)data)->magic == ESPNOW_MAGIC) &&
    (((espnow_header_t*)data)->type == ESPNOW_ACK);
}

bool EspNowClientPlus::sendData() {
  const uint8_t REPEAT = 5;
  const uint32_t GAP = 1; // 1 ms.

  uint8_t repeat = REPEAT;
  espnow_data_t data;
  uint32_t start;

  data.header.magic = ESPNOW_MAGIC;
  data.header.type = ESPNOW_DATA;
  data.header.num = ++_num;
  data.payload.id = ESP.getChipId();
  data.payload.uptime = millis();
  _received = false;
  while ((! _received) && repeat--) {
    if (esp_now->sendReliable(_server_mac, (uint8_t*)&data, sizeof(data), 1, GAP)) {
      start = millis();
      while ((! _received) && (millis() - start < GAP + 1)) {
        delay(1);
      }
    }
  }

  return _received;
}

static void halt(const __FlashStringHelper *msg) {
  Serial.println(msg);
  Serial.println(F("System halted!"));
  Serial.flush();
  if (led)
    led->setMode(LED_OFF);

  ESP.deepSleep(0);
}

static void reboot(const __FlashStringHelper *msg) {
  Serial.println(msg);
  Serial.println(F("System restarted!"));
  Serial.flush();
  if (led)
    led->setMode(LED_OFF);

  ESP.restart();
}


static String macToString(const uint8_t mac[]) {
  char str[18];

  sprintf_P(str, PSTR("%02X:%02X:%02X:%02X:%02X:%02X"), mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  return String(str);
}

void setup() {
  Serial.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);
  Serial.println();

  Serial.print(F("SDK version: "));
  Serial.println(ESP.getSdkVersion());

  led = new Led(LED_PIN, LED_LEVEL);

  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  {
    int8_t channel;
    uint8_t mac[6];
    int8_t rssi;
    uint32_t start;

    Serial.print(F("Waiting for ESP-NOW server"));
    channel = espNowFindServer(mac, &rssi);
    if (! channel) {
      const uint32_t WAIT_SERVER_TIMEOUT = 15000; // 15 sec.

      led->setMode(LED_4HZ);
      start = millis();
      while ((! channel) && (millis() - start <= WAIT_SERVER_TIMEOUT)) {
        led->delay(1000);
        Serial.print('.');
        channel = espNowFindServer(mac, &rssi);
      }
    }
    if (channel) {
      Serial.print(F(" OK (on channel "));
      Serial.print(channel);
      Serial.print(F(", mac: "));
      Serial.print(macToString(mac));
      Serial.print(F(", rssi: "));
      Serial.print(rssi);
      Serial.println(F(" dB)"));
      esp_now = new EspNowClientPlus(channel, mac);
      Serial.print(F("ESP-NOW client "));
      if (esp_now->begin()) {
        Serial.println(F("started"));
        led->setMode(LED_1HZ);
      } else {
        reboot(F("FAIL!"));
      }
    } else {
      reboot(F(" FAIL!"));
    }
  }
}

void loop() {
  const uint32_t SEND_PERIOD = 5000; // 5 sec.
  const uint8_t MAX_ERRORS = 5;

  static uint32_t lastSend = 0;
  static uint8_t errors = 0;

  if ((! lastSend) || (millis() - lastSend >= SEND_PERIOD)) {
    Serial.print(F("Sending DATA packet "));
    if (((EspNowClientPlus*)esp_now)->sendData()) {
      Serial.println(F("OK"));
      errors = 0;
    } else {
      Serial.println(F("FAIL!"));
      if (++errors >= MAX_ERRORS)
        reboot(F("Too many errors (connection lost)!"));
    }
    lastSend = millis();
  }
  led->delay(1);
}
